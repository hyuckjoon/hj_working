/*
필요 기능 
1. 기준날짜 앞  n일 뒤 n일 비교 (증감비율 , 증감 수 , 키워드 비중 )
2. windown 가 다 차지 않은경우 에러 메시지 출력 <- 오버스팩
*/

with max_update as ( 
select 
-- date_add ( 'day',1, current_date ) as last_update
 date_add ('day',cast ('{{window}}' as integer)-1, date( '{{date_v}}' )  )  as  af_end
, date('{{date_v}}') af_start
, date_add ('day',-1, date( '{{date_v}}' )  )  as  bf_end
, date_add ('day',-(cast ('{{window}}' as integer)), date( '{{date_v}}' )  )  as  bf_start
) 

, srp_keyword as (
select trim(search_word) search_word
, date(p_ymd) date_1
, if ( date(p_ymd) between date(af_start) and date( af_end) 
        , 'AF'
        , if ( date(p_ymd) between date(bf_start) and date( bf_end) 
                ,'BF'
                )
    )as period 
        
,cnt search_cnt
, result_count*cnt sum_result_count
from s_data.daily_srp_ctr a
join g_data.calendar c on a.dt =c.date_v
cross join max_update
where p_ymd >= '2021-06-01'
and ((platform in ('Android','iOS') and event_name ='bg') or (platform in ('MWeb','Web') and event_name ='page_view'))
-- page_view 는 검색or 필터 -> 페이지로드 시 적재 
-- bg 는 검색 or 필터시 발생 
-- web은 bg 로 트레킹 불가 
and date(p_ymd) >= date(bf_start)
and date(p_ymd) <= date(af_end)
and search_word is not null 
-- group by 1, 2
)
-- -- 검증용 쿼리 
-- select period 
-- , count ( distinct date_1 )
-- , min(date_1)
-- , max(date_1 )
-- from srp_keyword
-- group by 1 
, raw_1 as ( 
select 
search_word
, cast(sum( case when period = 'AF' then search_cnt else 0  end ) as double ) as af_cnt 
,  cast(sum(case when period = 'BF' then search_cnt else 0  end ) as double )  as bf_cnt 
, cast(sum (  case when period = 'AF' then sum_result_count else 0   end ) as double ) af_result_cnt
-- /sum( case when period = 'AF' then search_cnt else 0  end )  as af_result_avg 
, cast( sum( case when period = 'BF' then sum_result_count else 0  end )as double )  bf_result_cnt
-- /sum(case when period = 'BF' then search_cnt else 0  end ) ) as bf_result_avg   
from srp_keyword
group by 1 

-- order by 2 desc 
)
, raw_2 as ( 
select 
search_word
, af_cnt
, bf_cnt 
-- , af_result_cnt
-- , bf_result_cnt
, case when af_cnt = 0 then 0 else af_result_cnt end /
    af_cnt  af_result_avg
, case when bf_cnt = 0 then 0 else bf_result_cnt end /
    bf_cnt  bf_result_avg
from raw_1 
)
select
rank()over(order by af_cnt desc ) as rank
, search_word "검색어"
, replace( search_word , ' ', '') as "검색어_띄어쓰기없음"
,  cast ( bf_cnt as integer )  "이전검색횟수"
,  cast (af_cnt as integer )    "이후검색횟수"
,  case when  bf_cnt = 0 then 1 else cast(af_cnt as double )/cast (bf_cnt as double ) end  -1 "'검색횟수' 증감"
, '||' as _
, cast ( bf_result_avg as integer )  "이전 평균 작품수"
, cast ( af_result_avg  as integer ) "이후 평균 작품수"
,  case when  bf_result_avg = 0 then 1 else cast(af_result_avg as double )
/cast (bf_result_avg as double ) end  -1  "'평균 작품수' 증감"

from raw_2
order by rank 


-- nan 처리하기 
-- replace () 검색 기능 만들기 
-- 필터 부분 현업과 확인 후 추가 






