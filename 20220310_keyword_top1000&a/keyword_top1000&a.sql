/*

*/

with srp_keyword as (
select trim(search_word) search_word,sum(cnt) search_cnt, sum(result_count*cnt) sum_result_count
from s_data.daily_srp_ctr a
join g_data.calendar c on a.dt =c.date_v
where p_ymd >= '2021-06-01'
and ((platform in ('Android','iOS') and event_name ='bg') or (platform in ('MWeb','Web') and event_name ='page_view'))
-- page_view 는 검색or 필터 -> 페이지로드 시 적재 
-- bg 는 검색 or 필터시 발생 
-- web은 bg 로 트레킹 불가 
and p_ymd >= '{{1_date.start}}'
and p_ymd <= '{{1_date.end}}'
and search_word is not null 
group by 1
)

select rnk,search_word,search_cnt "검색횟수", sum_result_count/search_cnt "평균 검색결과수(작품수)"
from (
    select *, rank() over(order by search_cnt desc) rnk 
    from srp_keyword
    ) a
where 1=1 

order by 1
