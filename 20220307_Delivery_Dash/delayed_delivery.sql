/*
redash : http://redash.idus.io/queries/4582/source?p_date_v.start=2022-03-02&p_date_v.end=2022-03-10#7906
notion : https://www.notion.so/idus/26eabbadb8024381a31d4bf2da461dbf
bitBucket : https://bitbucket.org/hyuckjoon/hj_working/src/master/20220307_Delivery_Dash/
최대제작 발송기간 초과하는 배송건 추적 
*/
with holiday as (
/* 달력 테이블에서 휴일 테이블을 left join 해서 휴일만 추출함 */
select a.date_v as holiday
-- , a.dow_en
-- , b.date_v h_date
from g_data.calendar a 
left join g_data.calendar_holiday b on a.date_v = b.date_v 
where 1=1 
    and (a.dow_en in( 'Sun', 'Sat')  or b.date_v is not null)
)
, holiday_join_uniqN as (
-- holiday 때문에 o.id에서 중복 발생 
select 
o.ordernum  -- ordernum 이랑 order uuid 차이점 ? 
, o.created
, o.artist_uuid
, o.artist_name
, o.product_uuid
-- , rand() rand
, s.making_term
, date_diff( 'day', o.created , now() ) as dd_created_now
, h.holiday
from bk_idusme.order_total  o 
left join bk_idusme.s_salespolicy s on s.s_useruuid = o.artist_uuid
left join holiday h on h.holiday between date ( o.created) and date ( now())
where 1=1
    and o.ship_date is null 
    -- 배송되지 않고 취소되지 않은 경우 
    and date( o.created) >= date( '2021-01-01')
    and date( o.created) < date( '2023-01-01')
    -- holiday 업데이트되면 풀어주세요
        -- 2022-12-31 까지만 사용 가능합니다.   
    and date(o.created) between date('{{date_v.start}}') and date('{{date_v.end}}' ) -- data param
    and o.lastest_state not in (4 , 10) -- * 환불/취소건 제거
    and o.user_id not in (20) -- * 아이디어스 개발유저 user_id 제거
    and o.artist_id <> 1919927 -- * 작가스토어 id 제거
-- order by rand
-- limit 10 
)
, raw_product  as  ( 
select 
-- distinct pk ordernum & product_uuid 
ordernum

, created
, artist_uuid
, artist_name
, product_uuid
, making_term
, dd_created_now
-- aggregation
, count( holiday) as h_day_cnt 

from holiday_join_uniqN
group by 1,2,3,4, 5 , 6 ,7
)
, raw as (  -- 여기서 리뷰하기 편합니다 
select 
-- distinct pk ordernum & product_uuid 
ordernum
, concat ( 'https://pentagon.idus.com/orders/search?ordernum=' , ordernum ) as pentagon_url
, created
, date ( created) create_date 
, artist_uuid
, artist_name
, product_uuid
, concat (  'https://www.idus.com/w/product/'  ,   product_uuid  ) as prdt_link
, making_term
, dd_created_now
, h_day_cnt

-- calc
,  if  ( ( dd_created_now - h_day_cnt ) <= 0 , 0 , dd_created_now - h_day_cnt  )  as net_leadtime
        -- net_leadtime 은 zreo 이면 안된다 
, -making_term + ( dd_created_now - h_day_cnt  )  as over_mTerm
, if ( (-making_term + ( dd_created_now - h_day_cnt  ) ) >=  0 
    ,  1
    ,  0 
    )  over_flag -- 1 : 위험 , 2 : 문제없음 
from raw_product p 
where 1=1 
    and -making_term + ( dd_created_now - h_day_cnt  ) >= 0 

-- select 
-- ordernum , product_uuid  -- 이조건으로 distinct 함 
-- from raw 
-- group by 1 , 2
-- having count(*)> 1 


-- 마지막에 distinct check ! 
-- limit 제거'
)
select 
create_date
, ordernum
, pentagon_url
, artist_uuid  -- as '한글가능?'불가능
, artist_name
, product_uuid
, prdt_link as product_link 
, making_term
, dd_created_now leadtime_created_now
, net_leadtime as leadtime_created_now_notHoliday
, over_mTerm as over_making_term 
from raw 
order by over_mTerm desc 



