--  https://backpackr.atlassian.net/browse/BDA-19
-- holiday 를 검색해 보세요! 
with 
top_order as (
    /* 특정 기간 내 주문에 대한 대카테고리 기준 작품별 GMV 산출 */
    select b.category_grp, b.category_name, a.product_uuid, max(a.product_name) as product_name, cast(sum(a.pay_total_price) as bigint) as gmv
    from bk_idusme.order_total a
    left join s_data.category_group b on a.product_category_uuid = b.category_uuid
    where a.lastest_state not in (4,10)
        and a.artist_id <> 1919927
        and a.user_id <> 20
        and a.product_name not like '%개인 결제' -- 개인결제 제외함
        and date(a.created) between date '{{dt.start}}' and date '{{dt.end}}'
    group by b.category_grp, b.category_name, a.product_uuid
    )

, top_order_ranking as (
    /* 대카테고리 기준 작품별 GMV 순위 기재 */
    select row_number() over(partition by category_grp order by gmv desc) as ranking, a.*
    from top_order a
    )

, top_order_ranking_1 as (
    /* 대카테고리 별로 상위 100개 작품 추출 */
    select *
    from top_order_ranking
    where ranking <= 100
    order by category_grp, ranking
    )

, top_order_ranking_detail as (
    select a.created, a.ordernum, a.product_uuid
        , b.ranking, b.category_grp, b.category_name
        , a.artist_uuid, a.artist_name, a.product_name
        , a.pay_total_price, a.option_total_itemcnt, ship_system_date
    from bk_idusme.order_total a
    left join top_order_ranking_1 b on a.product_uuid = b.product_uuid
    where a.lastest_state not in (4,10)
        and a.artist_id <> 1919927
        and a.user_id <> 20
        and date(a.created) between date '{{dt.start}}' and date '{{dt.end}}'
        and b.product_uuid is not null
    )

, holiday as (
    /* 달력 테이블에서 휴일 테이블을 left join 해서 휴일만 추출함 */
    select a.date_v as holiday
    from g_data.calendar a 
    left join g_data.calendar_holiday b on a.date_v = b.date_v 
    where a.date_v between date('{{dt.start}}') and  date('{{dt.end}}') + interval '100' day
        and (a.dow_en = 'Sun' or b.date_v is not null)
    )

, delivery as (
    /* 주문건의 구매일자와 송장 번호 입력 일자 사이에 휴일이 있는 경우, holiday 컬럼에 행 별로 product_uuid에 해당 휴일이 기재됨. 
      한 작품에 여러개 행이 있는 게 많음 */
    select date(created) as dd
        , a.ordernum 
        , a.product_uuid 
        , a.category_grp
        , a.ranking
        , a.category_name
        , a.artist_uuid
        , a.artist_name
        , a.product_name
        , cast(pay_total_price as bigint) as pay_total_price
        , option_total_itemcnt
        , date(a.created) as purchase_date 
        , date(a.ship_system_date) as shipment_date 
        , date_diff('day', date(a.created), date(a.ship_system_date)) as day_diff
        , b.holiday
    from top_order_ranking_detail a 
    left join holiday b on b.holiday between date(a.created) and date(a.ship_system_date)
    )

, delivery_1 as (
    /* holiday의 개수를 count 해서 data_diff로 구한 총 발송까지의 기간에서 holiday의 개수를 빼줌. 그래서 working_day로 며칠 걸렸는지 확인. 
      주문 건의 작품 기준으로 group by 함. 결론적으로 주문 건 별로 작품의 working day 기준 배송일을 산출함 */
    select dd, ordernum, product_uuid
        , min(category_grp) as category_grp
        , min(ranking) as ranking
        , min(category_name) as category_name
        , min(artist_uuid) as artist_uuid
        , min(artist_name) as artist_name
        , min(product_name) as product_name
        , min(pay_total_price) as pay_total_price
        , min(option_total_itemcnt) as option_total_itemcnt
        , min(purchase_date) as purchase_date
        , min(shipment_date) as shipment_date
        , min(day_diff) as total_day
        , count(holiday) as holiday_cnt
        , min(day_diff) - count(holiday) as working_day_cnt
    from delivery 
    group by dd, ordernum, product_uuid 
    )

, live as (
    /* product 테이블의 일별 로그로 노출 여부 확인. (하루에 한 번 로그를 찍어서 로그 시점 기준의 노출 여부임) */
    select a.dd, a.ordernum, a.product_uuid
        , a.category_grp, a.ranking, a.category_name
        , a.artist_uuid, a.artist_name, a.product_name
        , a.pay_total_price, a.option_total_itemcnt, a.purchase_date, a.shipment_date, a.total_day, a.holiday_cnt, a.working_day_cnt
        , case when b.issale = 0 then 'Y' -- 비노출
              when b.issale = 1 then 'N' -- 노출
              else 'others' end as delive
        , b.issale, b.isvisible
    from delivery_1 a 
    join bh_idusme.product b on a.product_uuid = b.productuuid and a.dd = date(date_parse(p_ymd, '%Y%m%d'))
    )

/* 카테고리 정보 입력하고 gmv, 주문수 등을 계산함 */
select dd, product_uuid
    , min(category_grp) as category_grp
    , min(ranking) as ranking
    , min(category_name) as category_name
    , min(artist_uuid) as artist_uuid
    , min(artist_name) as artist_name
    , min(product_name) as product_name
    , sum(pay_total_price) as gmv
    , count(1) as order_cnt
    , sum(option_total_itemcnt) as product_order_cnt
    , avg(working_day_cnt) as avg_delivery_day
    , min(delive) as delive, min(issale) as issale, min(isvisible) as isvisible
from live
group by dd, product_uuid
order by dd, product_uuid



---
-- 형은님이 추가해주신 레퍼런스
with 
holiday as (
    select a.date_v as h_day
    from g_data.calendar a 
    left join g_data.calendar_holiday b on a.date_v = b.date_v 
    where a.date_v between date('{{date}}') - interval '100' day and date('{{date}}')
        and (a.dow_en = 'Sun' or b.date_v is not null)
    ), 
list0 as (
    select a.ordernum 
        , a.artist_uuid
        , a.product_uuid 
        , a.created as pur_date 
        , a.ship_system_date as ship_date 
        , date_diff('day' ,a.created, a.ship_system_date) as day_diff
        , b.h_day
    from bk_idusme.order_total a 
    left join holiday b on b.h_day between date(a.created) and date(a.ship_system_date)
    where date(a.ship_system_date) = date('{{date}}')
        and lastest_state not in (4, 10)
    ), 
list1 as (
    select ordernum, product_uuid
        , min(ship_date) as ship_date
        , min(artist_uuid) as artist_uuid
        , min(day_diff) as day_diff, count(h_day) as n_hday
        , min(day_diff) - count(h_day) as working_days
    from list0 
    group by ordernum, product_uuid 
    ), 
list2 as (
    select a.ship_date
        , a.ordernum 
        , a.artist_uuid
        , a.product_uuid 
        , a.working_days
        , b.making_term 
    from list1 a 
    left join bk_idusme.s_salespolicy b on a.artist_uuid = b.s_useruuid 
    where a.working_days > (making_term + 5)
    ), 
list3 as (
    select a.ship_date
        , a.ordernum
        , a.artist_uuid
        , c.artistname
        , d.company_name
        , a.product_uuid
        , b.name
        , a.working_days 
        , a.making_term
        , concat('https://pentagon.idus.com/orders/search?ordernum=', a.ordernum) as pentagon_url
    from list2 a  
    left join bk_idusme.product b on a.product_uuid = b.productuuid 
    left join bk_idusme.user c on a.artist_uuid = c.uuid
    left join bk_idusme.artist_info d on c.id = d.user_id -- > artistname 이 마스킹되어서 company_name 을 보완하기 위하여 추가된 내용 
    ) 
select *
from list3 


--근모님이 추가해주신 레퍼런스 
with holiday as (
    select * from (values ('2021-10-31'), ('2021-11-01')) as t (holiday) -- 공휴일 예시
),

sample as (
    select *
    from bk_idusme.order_total
    where date_format(created, '%Y-%m-%d') between '2021-10-01' and '2021-10-31' -- 대상기간 예시
    limit 1 -- 레코드 1개만 샘플링
),

base as (
    select *
    from sample a
    left join holiday b on b.holiday between date_format(a.created, '%Y-%m-%d') and '2021-12-31' -- 현재일이 21년 12월 31일이라 가정하고, 주문일로부터 현재일 사이에 포함된 공휴일만 left join
)

select id as order_total_id, date_diff('day', created, date'2021-12-31' + interval '1' day) as total_days, count(id) as holidays -- 주문일과 현재일 사이의 일수에서 공휴일수를 차감.   
from base
group by 1, 2 
