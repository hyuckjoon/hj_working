
-- ㅇㅇ
-- select 

delete from s_data.order_total_attribution where p_ymd = '{{date_v}}';

insert into s_data.order_total_attrib
with 
list as (
    select event_time as created
        , customer_user_id as user_id 
        , appsflyer_id 
        , media_source, campaign, af_adset, af_ad 
        , try_cast(json_extract(replace(replace(replace(event_value, '"[', '['),']"', ']'), '\"', '"'), '$.af_content_id') as ARRAY<varchar>) as uuid 
    from b_ext.af_data_locker_v2
    where dt = '{{date_v}}'
        and t in ('inapps', 'inapps_retargeting')
        and event_name in ('af_purchase', 'Purchase_First')
        and is_primary_attribution = 'true'
        and media_source != 'organic'
    ), 
pivot as (
    select date_format(created, '%Y%m%d%H') as purchase_ymdh
        , created 
        , appsflyer_id
        , user_id 
        , media_source, campaign, af_adset, af_ad
        , p_uuid  
    from list 
    cross join unnest(uuid) as t(p_uuid)
    ), 
order_tbl as (
    select date_format(c, '%Y%m%d%H') as purchase_ymdh, created, user_id, user_uuid, id, ordernum, product_uuid
    from bk_idusme.order_total 
    where created between date'{{date_v}}' and date'{{date_v}}' + interval '1' day 
        and lastest_state not in (4, 10)
    )
    
select distinct b.id as order_total_id, b.ordernum, b.user_id, b.created, b.product_uuid





    , a.appsflyer_id, a.media_source, a.campaign, a.af_adset, a.af_ad 
    
    
    
    , '{{date_v}}' as p_ymd 
    -- 여기가 변경사항 
from pivot a 
join order_tbl b on (a.user_id = cast(b.user_id as varchar) or a.user_id = b.user_uuid) and a.purchase_ymdh = b.purchase_ymdh and a.p_uuid = b.product_uuid;
   