/*
작성 : 
수정 : 권혁준 2022-03-15 

*/
with log as (
select *
,lag(page_name) over(partition by session_id order by created_ts) as previous 
,lag(json_extract_scalar(property, '$.showroom_id')) over(partition by session_id order by created_ts) as showroom_id 
from b_client_log.idus_daily
where p_ymd >= date_format(date('{{date.start}}'), '%Y%m%d') and p_ymd <= date_format(date('{{date.end}}'), '%Y%m%d')
and platform in ('Android','iOS','Web','MWeb')
and event_name in ('page_view','resume','click')
)

, showroom_click as (
select format_datetime(created_ts,'yyyy-MM-dd') dt
,showroom_id
,cgnt_id
-- ,user_id
,json_extract_scalar(property, '$.product_uuid')  product_uuid
,count(1) click
,min(created_ts) min_ts
from log
where 1=1
and page_name ='product_detail' and event_name ='page_view'
and previous ='showroom'
group by format_datetime(created_ts,'yyyy-MM-dd') 
,showroom_id
,cgnt_id
-- ,user_id
,json_extract_scalar(property, '$.product_uuid')  
)


, ordercomplete_uv as (
select p_ymd,showroom_id ,count(distinct a.cgnt_id) user
from s_data.daily_product_ordercomplete a 
join showroom_click b on a.cgnt_id=b.cgnt_id and a.created_ts >= b.min_ts and a.product_uuid=b.product_uuid
where p_ymd >= '{{date.start}}' and  p_ymd <= '{{date.end}}'
group by 1,2
)
, showroom_uv as (
select format_datetime(created_ts,'yyyy-MM-dd') dt
,showroom_id
,count(distinct case when page_name='showroom' then cgnt_id end) visit_uv
,count(distinct case when page_name ='product_detail' and event_name ='page_view' and previous ='showroom' then cgnt_id end) pdp_click_uv
from log
where 1=1

group by 1,2
)

select a.*,b.user as order_user, b.user*100.00 / visit_uv "비중(%)"
from showroom_uv a
left join ordercomplete_uv b on b.p_ymd=a.dt and a.showroom_id=b.showroom_id
